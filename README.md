# MVP

A demo app showing my interpretation of the MVP (Model-View-Presenter) architecture.

## Feature: DateConverter
The feature in this demo is a Date Converter. There is an input to allow you to enter an epoch (a time in seconds since Jan 1, 1970), and multiple display elements to display this date in different ways. There is also a button that can be used to "refresh" the time back to the current time, which will update all the other elements as well.

### (M)odel
A plain an simple `struct` which is `Codable` and `Equatable`.

    struct DateConverterModel: Codable, Equatable {
        var epoch: TimeInterval?
    }

### (V)iew (and View Controller)
Also fairly straight-forward. For this demo the UI is laid out from top to bottom with vertical spacing with the following outlets:

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iso8601Label: UILabel!
    @IBOutlet weak var epochInputTextField: UITextField!
    @IBOutlet weak var refreshButton: UIButton!

The pickers have interaction disabled, so that they can be for display only. Other than that, there's nothing fancy about any of these elemenets.

Updating the UI from the `Presenter` is also simple:

	extension ViewController {
	    private func updateUI(presenter: DateConverterPresenterType) {
	        dateLabel.text = presenter.outputs.dateText
	        iso8601Label.text = presenter.outputs.iso8601Text
	        epochInputTextField.text = "\(presenter.outputs.epochTruncated)"
	        datePicker.setDate(Date(timeIntervalSince1970: TimeInterval(presenter.outputs.epochRaw)), animated: true)
	        timePicker.setDate(Date(timeIntervalSince1970: TimeInterval(presenter.outputs.epochRaw)), animated: true)
	    }
	}

The `updateUI` function is called from a delegate function of the `Presenter`:

	extension ViewController: DateConverterPresenterOutputsDelegate {
	    func epochDidChange(_ presenter: DateConverterPresenterType) {
	        DispatchQueue.main.async {
	            self.updateUI(presenter: presenter)
	        }
	    }
	}

There's some other code in the `ViewController` to handle the keyboard and the text field interactions, but it's minimal and not relevant to MVP.

### (P)resenter
There are 3 parts to the Presenter file which, due to the limitations of access control in Swift, must all be in the same file. Those 3 parts are: the `Presenter` itself, `Repository` and `Data Access Object` initialization, and the `Presenter` `Factory`.

#### Presenter
The `Presenter`, as a concept, relies on interfaces and a `Factory` to hide the implementation details of the `Presenter` itself. What the outside world sees is actually a `PresenterType`. It is composed of 3 parts: inputs, outputs, and a delegate. The general structure will look like this:

	protocol DateConverterPresenterInputs {}
	protocol DateConverterPresenterOutputs {}
	protocol DateConverterPresenterOutputsDelegate: AnyObject {}
		
	protocol DateConverterPresenterType {
	    var inputs: DateConverterPresenterInputs { get }
	    var outputs: DateConverterPresenterOutputs { get }
	    var delegate: DateConverterPresenterOutputsDelegate? { get set }
	}

The concrete `Presenter` will look like this:

    private class DateConverterPresenter: DateConverterPresenterType, DateConverterPresenterInputs, DateConverterPresenterOutputs {
	    var inputs: DateConverterPresenterInputs { self }
	    var outputs: DateConverterPresenterOutputs { self }
	    weak var delegate: DateConverterPresenterOutputsDelegate?
	    ...
	}

Notice how it is its own `inputs` and `outputs`? This is mostly sugaring so that you can invoke functions in a sort of namespace that is internal to the `PresenterType`. At the call site, you'd invoke an input function like `presenter.inputs.refreshButtonPressed()`, and you'd get an output variable like `presenter.outputs.dateText`. The `PresenterOutputsDelegate` receives communications about changes to the model.

Let's look at some of the details of those protocols with regards to this `Date Converter` feature.

	protocol DateConverterPresenterInputs {
	    func didAppear()
	    func refreshButtonPressed()
	    func epochRawInputChanged(to epoch: Int)
	}

The inputs are the ways in which you can interact with the scene. The scene can appear (`didAppear`), you can tap on the refresh button in the UI (`refreshButtonPressed`), or you can change the input in the text field (`epochRawInputChanged`).

	protocol DateConverterPresenterOutputs {
	    // MARK: Model
	    var dateText: String { get }
	    var iso8601Text: String { get }
	    var epochRaw: TimeInterval { get }
	    var epochTruncated: Int { get }
	}

The outputs are all of the transformations of the Model that you'd want to have, and any business logic that goes along with that (though this example doesn't really have any business logic in it).

	protocol DateConverterPresenterOutputsDelegate: AnyObject {
	    // MARK: Model
	    func epochDidChange(_ presenter: DateConverterPresenterType)
	}
	
	extension DateConverterPresenterOutputsDelegate {
	    // MARK: Model
	    func epochDidChange(_ presenter: DateConverterPresenterType) {}
	}

The outputs delegate receives communications about changes to the model. This will typically be the View Controller so that it can update the View.

Let's look at the implementation details of the outputs.

	private class DateConverterPresenter ... {
		...
		var dateText: String { dateFromEpoch.localTimeShort }
		var iso8601Text: String { dateFromEpoch.iso8601 }
		var epochRaw: TimeInterval { dataAccessObject.model?.epoch ?? 0 }
		var epochTruncated: Int { Int(epochRaw) }
		    
		private var dataAccessObject: DateConverterDAO
		private var dateFromEpoch: Date { Date(timeIntervalSince1970: epochRaw) }
		    
		init(dataAccessObject: DateConverterDAO, delegate: DateConverterPresenterOutputsDelegate? = nil) {
		    self.dataAccessObject = dataAccessObject
		    self.delegate = delegate
		}
		...
	}

A `Presenter` has 2 properties injected into it: a `Data Access Object`(DAO), and a delegate. If we look at the property `epochRaw` we see that it is using the injected `dataAccessObject` to get a `model` object and get the `epoch` value off of it. As you might expect, the `model` is a `DateConverterModel`. So what is the `dataAccessObject` and what is its purpose?

#### Repository and Data Access Objects
A `Data Access Object` is responsible for obfuscating where the model is actually coming from. When I first created this interpretation of MVP it was to bring together Firebase Remote Config, User Defaults, and local data from a dictionary all into one harmonious structure. I realized that all of these source of data were really just doing CRUD operations (**C**reate, **R**ead, **U**pdate, **D**elete) using key/value pairs. So I abstracted that functionality into an interface that I call a `Repository`. A `Data Access Object` is just a box around a `Repository`:

	class DataAccessObject {
	    private(set) var repo: Repository
	    
	    init(repo: Repository) {
	        self.repo = repo
	    }
	}

See the Respository directory for how UserDefaults, NSMutableDictionary, and the standard [AnyHashable: Any] dictionary can all conform to my `Repository` interface and be used interchangeably.

Not caring about the underlying storage container of the data is great, and a `Repository` in a `DataAccessObject` takes care of that, but in my case of Remote Config, User Defaults, and local backups, I needed a way of saying, 

> "check Remote Config first, and if you don't find it there, check User Defaults, and if you don't find it there, lastly grab the value from my local default values"

To do this, I extended `Repository` and `DataAccessObject` into what I call `CascadingRepository` and `CascadingDataAccessObject`. A `CascadingRepository` is a `Repository` that just acts on an array of prioritized `Repository`s:

	struct CascadingRepository: Repository {
	    private var prioritizedRepos: [Repository]?
	    
	    init(prioritizedRepos: [Repository]? = nil) {
	        self.prioritizedRepos = prioritizedRepos
	    }
	}

And a `CascadingDataAccessObject`, similarly, is still just a box around a `CascadingRepository`:

	class CascadingDataAccessObject {
	    private(set) var cascadingRepo: CascadingRepository
	    
	    init(cascadingRepo: CascadingRepository) {
	        self.cascadingRepo = cascadingRepo
	    }
	}

These underlying pieces aren't really part of MVP, though, but they are necessary to understanding how I use them in a `Presenter`. As part of an MVP feature, a corresponding DAO is created to access the model and to update the model. This is what the DAO looks for the Date Converter feature:

	class DateConverterDAO: CascadingDataAccessObject {
	    enum Keys {
	        case model
	        
	        private var base: String { "date_converter_" }
	        private var key: String {
	            switch self {
	            case .model: return "model"
	            }
	        }
	        
	        var repoKey: String { base + key }
	    }
	    
	    var model: DateConverterModel? {
	        get { cascadingRepo.value(forKey: Keys.model.repoKey) }
	        set { cascadingRepo.update(encodableValue: newValue, forKey: Keys.model.repoKey) }
	    }
	}

`DateConverterDAO` inherits from `CascadingDataAccessObject`. It then exposes a `Keys` enum, and a set of properties that line up with the cases in the `Keys` enum (in this case, there is only the `model`). The `Keys` is responsible for assembling the string keys that will be used to look up the entity. The `model` property is an optional `DateConverterModel`, and if you try to `get` it, it will use the `cascadingRepo` to try to find a `DateConverterModel` in each of the prioritized repos under the given key. If you `set` the value, it will attempt to update the repos.

(You undoubtedly will have questions about what it really means to Create, Read, Update, or Delete an associated kvp within an array of repos, and that is a totally valid argument that this example project does not attempt to solve, but all of the functionalities imaginable are possbile at the `Repository` level, and can be implemented as such at the DAO level)

#### Presenter Factory
The Factory pattern aims to hide the implementation details of concrete classes and instead just produce some interface-conforming type. While there is only a single concrete `Presenter` class in my MVP, the source and contents of the data that sits underneath the DAO can and should be controllable. It is not shown in this example, but you can create multiple `Presenters`, like a `real` and a `mock`, and switch which one to use at the `Factory` and the rest of the code stays exactly the same.

	enum DateConverterPresenterFactory {
	    typealias PresenterFactoryType = DateConverterPresenterType
	    fileprivate typealias PresenterFactoryConcreteType = DateConverterPresenter
	    typealias DAOType = DateConverterDAO
	    typealias OutputsDelegateFactoryType = DateConverterPresenterOutputsDelegate
	
	    enum PresenterType {
	        case real
	    }
	    
	    static func getPresenter() -> PresenterFactoryType {
	        return realPresenter
	    }
	    
	    static func getPresenter(type: PresenterType) -> PresenterFactoryType {
	        switch type {
	        case .real: return realPresenter
	        }
	    }
	    
	    static func makePresenter(type: PresenterType, delegate: OutputsDelegateFactoryType? = nil) -> PresenterFactoryType {
	        switch type {
	        case .real:
	            return PresenterFactoryConcreteType(
	                dataAccessObject: DateConverter.Real.dataAccessObject,
	                delegate: delegate
	            )
	        }
	    }
	    
	    static func makePresenter(dataAccessObject: DAOType, delegate: OutputsDelegateFactoryType? = nil) -> PresenterFactoryType {
	        return PresenterFactoryConcreteType(
	            dataAccessObject: dataAccessObject,
	            delegate: delegate
	        )
	    }
	}

The factory is basically the same for all Features. For this reason, it makes use of a few typealiases, which are the only pieces that need to be adapted from one feature to the next.

## Interaction Cycles
### Raw Entry
When the text field text changes, this is the cycle of functions that get called:

	ViewController.swift *** textFieldDidChange(_:)[67]
	DateConverterPresenter.swift *** epochRawInputChanged(to:)[101]
	DateConverterPresenter.swift *** updateEpoch(to:)[81]
	ViewController.swift *** epochDidChange(_:)[90]
	ViewController.swift *** updateUI(presenter:)[78]

1. The View Controller handles the input on the UI element and notifies the presenter of the input
2. The Presenter uses a helper function
3. The helper function updates the model and notifies the delegate
4. The View Controller is the delegate and receives communication that the model changed
5. The View Controller updates the UI

### Refresh Button
When the refresh button is tapped, this is the cycle of functions that get called:

	ViewController.swift *** refreshButtonTapped(_:)[61]
	DateConverterPresenter.swift *** refreshButtonPressed()[95]
	DateConverterPresenter.swift *** updateEpoch(to:)[81]
	ViewController.swift *** epochDidChange(_:)[90]
	ViewController.swift *** updateUI(presenter:)[78]

The flow is very similar.
