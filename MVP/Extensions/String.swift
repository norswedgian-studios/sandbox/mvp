//
//  String.swift
//  MVP
//
//  Created by Tim Fuqua on 8/18/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

extension String {
    init?<T: LosslessStringConvertible>(optional: T?) {
        guard let optional = optional else { return nil }
        self = String(optional)
    }
}
