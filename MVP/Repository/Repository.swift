//
//  Repository.swift
//  MVP
//
//  Created by Tim Fuqua on 8/17/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: -
// MARK: -
// MARK: - Repository
protocol Repository {
    // Create (C)
    func create(stringValue value: String, forKey key: String)
    func create(boolValue value: Bool, forKey key: String)
    func create(intValue value: Int, forKey key: String)
    func create(doubleValue value: Double, forKey key: String)
    func create<T: Encodable>(encodableValue value: T, forKey key: String)
    
    // Read (R)
    func stringValue(forKey key: String) -> String?
    func boolValue(forKey key: String) -> Bool?
    func intValue(forKey key: String) -> Int?
    func doubleValue(forKey key: String) -> Double?
    func value<T: Decodable>(forKey key: String) -> T?
    
    // Update (U)
    func update(stringValue value: String, forKey key: String)
    func update(boolValue value: Bool, forKey key: String)
    func update(intValue value: Int, forKey key: String)
    func update(doubleValue value: Double, forKey key: String)
    func update<T: Encodable>(encodableValue value: T, forKey key: String)

    // Delete (D)
    @discardableResult
    func deleteValue(forKey key: String) -> Bool
}

extension Repository {
    // Create (C)
    func create(stringValue value: String, forKey key: String) {}
    func create(boolValue value: Bool, forKey key: String) {}
    func create(intValue value: Int, forKey key: String) {}
    func create(doubleValue value: Double, forKey key: String) {}
    func create<T: Encodable>(encodableValue value: T, forKey key: String) {}
    
    // Read (R)
    func stringValue(forKey key: String) -> String? { nil }
    func boolValue(forKey key: String) -> Bool? { nil }
    func intValue(forKey key: String) -> Int? { nil }
    func doubleValue(forKey key: String) -> Double? { nil }
    func value<T: Decodable>(forKey key: String) -> T? { nil }
    
    // Update (U)
    func update(stringValue value: String, forKey key: String) {}
    func update(boolValue value: Bool, forKey key: String) {}
    func update(intValue value: Int, forKey key: String) {}
    func update(doubleValue value: Double, forKey key: String) {}
    func update<T: Encodable>(encodableValue value: T, forKey key: String) {}
    
    // Delete (D)
    @discardableResult
    func deleteValue(forKey key: String) -> Bool { false }
}
