//
//  MutableDictionaryRepository.swift
//  MVP
//
//  Created by Tim Fuqua on 8/17/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - MutableDictionaryRepository
class MutableDictionaryRepository: MutableDictionaryRepositoryType {
    // MARK: MutableDictionaryRepositoryType vars
    private(set) var dict: NSMutableDictionary
    
    // MARK: inits
    init(dict: NSMutableDictionary = [:]) {
        self.dict = dict
    }
}
