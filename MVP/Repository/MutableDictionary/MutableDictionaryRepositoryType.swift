//
//  MutableDictionaryRepositoryType.swift
//  MVP
//
//  Created by Tim Fuqua on 8/17/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - MutableDictionaryRepositoryType
protocol MutableDictionaryRepositoryType: Repository {
    var dict: NSMutableDictionary { get }
}

// MARK: - Create (C)
extension MutableDictionaryRepositoryType {
    func create(stringValue value: String, forKey key: String) {
        dict[key] = value
    }
    
    func create(boolValue value: Bool, forKey key: String) {
        dict[key] = value
    }
    
    func create(intValue value: Int, forKey key: String) {
        dict[key] = value
    }
    
    func create(doubleValue value: Double, forKey key: String) {
        dict[key] = value
    }
    
    func create<T>(encodableValue value: T, forKey key: String) where T: Encodable {
        dict[key] = value
    }
}

// MARK: - Read (R)
extension MutableDictionaryRepositoryType {
    func stringValue(forKey key: String) -> String? {
        return dict[key] as? String
    }
    
    func boolValue(forKey key: String) -> Bool? {
        return dict[key] as? Bool
    }
    
    func intValue(forKey key: String) -> Int? {
        return dict[key] as? Int
    }
    
    func doubleValue(forKey key: String) -> Double? {
        return dict[key] as? Double
    }
    
    func value<T>(forKey key: String) -> T? where T: Decodable {
        return dict[key] as? T
    }
}

// MARK: - Update (U)
extension MutableDictionaryRepositoryType {
    func update(stringValue value: String, forKey key: String) {
        dict[key] = value
    }
    
    func update(boolValue value: Bool, forKey key: String) {
        dict[key] = value
    }
    
    func update(intValue value: Int, forKey key: String) {
        dict[key] = value
    }
    
    func update(doubleValue value: Double, forKey key: String) {
        dict[key] = value
    }
    
    func update<T>(encodableValue value: T, forKey key: String) where T: Encodable {
        dict[key] = value
    }
}

// MARK: - Delete (D)
extension MutableDictionaryRepositoryType {
    func deleteValue(forKey key: String) -> Bool {
        if dict.object(forKey: key) != nil {
            dict.removeObject(forKey: key)
            return true
        }
        
        return false
    }
}
