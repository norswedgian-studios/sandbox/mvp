//
//  CascadingRepository.swift
//  MVP
//
//  Created by Tim Fuqua on 8/17/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - CascadingRepository
struct CascadingRepository: Repository {
    // MARK: injected vars
    private var prioritizedRepos: [Repository]?
    
    // MARK: inits
    init(prioritizedRepos: [Repository]? = nil) {
        self.prioritizedRepos = prioritizedRepos
    }
}

// MARK: - Read (R)
extension CascadingRepository {
    func stringValue(forKey key: String) -> String? {
        var potential: String?
        
        prioritizedRepos?.forEach { repo in
            if potential == nil, let value: String = repo.stringValue(forKey: key) {
                potential = value
                return
            }
        }
        
        return potential
    }
    
    func boolValue(forKey key: String) -> Bool? {
        var potential: Bool?
        
        prioritizedRepos?.forEach { repo in
            if potential == nil, let value: Bool = repo.boolValue(forKey: key) {
                potential = value
                return
            }
        }
        
        return potential
    }
    
    func intValue(forKey key: String) -> Int? {
        var potential: Int?
        
        prioritizedRepos?.forEach { repo in
            if potential == nil, let value: Int = repo.intValue(forKey: key) {
                potential = value
                return
            }
        }
        
        return potential
    }
    
    func doubleValue(forKey key: String) -> Double? {
        var potential: Double?
        
        prioritizedRepos?.forEach { repo in
            if potential == nil, let value: Double = repo.doubleValue(forKey: key) {
                potential = value
                return
            }
        }
        
        return potential
    }
    
    func value<T: Decodable>(forKey key: String) -> T? {
        var potential: T?
        
        prioritizedRepos?.forEach { repo in
            if potential == nil, let value: T = repo.value(forKey: key) {
                potential = value
                return
            }
        }
        
        return potential
    }
}

// MARK: - Update (U)
extension CascadingRepository {
    func update(stringValue value: String, forKey key: String) {
        prioritizedRepos?.forEach { $0.update(stringValue: value, forKey: key) }
    }
    
    func update(boolValue value: Bool, forKey key: String) {
        prioritizedRepos?.forEach { $0.update(boolValue: value, forKey: key) }
    }
    
    func update(intValue value: Int, forKey key: String) {
        prioritizedRepos?.forEach { $0.update(intValue: value, forKey: key) }
    }
    
    func update(doubleValue value: Double, forKey key: String) {
        prioritizedRepos?.forEach { $0.update(doubleValue: value, forKey: key) }
    }
    
    func update<T: Encodable>(encodableValue value: T, forKey key: String) {
        prioritizedRepos?.forEach { $0.update(encodableValue: value, forKey: key) }
    }
}
