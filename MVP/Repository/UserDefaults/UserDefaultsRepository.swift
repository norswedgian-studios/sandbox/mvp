//
//  UserDefaultsRepository.swift
//  MVP
//
//  Created by Tim Fuqua on 8/17/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - UserDefaultsRepository
class UserDefaultsRepository: UserDefaultsRepositoryType {
    static var `default` = UserDefaultsRepository()
    
    // MARK: UserDefaultsRepositoryType vars
    private(set) var defaults: UserDefaults
    
    // MARK: inits
    init(defaults: UserDefaults = UserDefaults.standard) {
        self.defaults = defaults
    }
}
