//
//  UserDefaultsRepositoryType.swift
//  MVP
//
//  Created by Tim Fuqua on 8/17/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - UserDefaultsRepositoryType
protocol UserDefaultsRepositoryType: Repository {
    var defaults: UserDefaults { get }
}

// MARK: - Create (C)
extension UserDefaultsRepositoryType {
    func create(stringValue value: String, forKey key: String) {
        defaults.set(value, forKey: key)
    }

    func create(boolValue value: Bool, forKey key: String) {
        defaults.set(value, forKey: key)
    }

    func create(intValue value: Int, forKey key: String) {
        defaults.set(value, forKey: key)
    }
    
    func create(doubleValue value: Double, forKey key: String) {
        defaults.set(value, forKey: key)
    }

    func create<T>(encodableValue value: T, forKey key: String) where T: Encodable {
        guard let encodedObject = try? JSONEncoder().encode(value) else { return }
        defaults.set(encodedObject, forKey: key)
    }
}

// MARK: - Read (R)
extension UserDefaultsRepositoryType {
    func stringValue(forKey key: String) -> String? {
        return defaults.string(forKey: key)
    }
    
    func boolValue(forKey key: String) -> Bool? {
        // Not using `bool(forKey:)` because it defaults to `false` if the key doesn't exist
        return defaults.object(forKey: key) as? Bool
    }
    
    func intValue(forKey key: String) -> Int? {
        // Not using `integer(forKey:)` because it defaults to `0` if the key doesn't exist
        return defaults.object(forKey: key) as? Int
    }
    
    func doubleValue(forKey key: String) -> Double? {
        // Not using `double(forKey:)` because it defaults to `0` if the key doesn't exist
        return defaults.object(forKey: key) as? Double
    }
    
    func value<T>(forKey key: String) -> T? where T: Decodable {
        guard let data = defaults.object(forKey: key) as? Data else { return nil }
        guard let decodedObject = try? JSONDecoder().decode(T.self, from: data) else { return nil }
        return decodedObject
    }
}

// MARK: - Update (U)
extension UserDefaultsRepositoryType {
    func update(stringValue value: String, forKey key: String) {
        defaults.set(value, forKey: key)
    }
    
    func update(boolValue value: Bool, forKey key: String) {
        defaults.set(value, forKey: key)
    }
    
    func update(intValue value: Int, forKey key: String) {
        defaults.set(value, forKey: key)
    }
    
    func update(doubleValue value: Double, forKey key: String) {
        defaults.set(value, forKey: key)
    }
    
    func update<T>(encodableValue value: T, forKey key: String) where T: Encodable {
        guard let encodedObject = try? JSONEncoder().encode(value) else { return }
        defaults.set(encodedObject, forKey: key)
    }
}

// MARK: - Delete (D)
extension UserDefaultsRepositoryType {
    @discardableResult
    func deleteValue(forKey key: String) -> Bool {
        if defaults.object(forKey: key) != nil {
            defaults.removeObject(forKey: key)
            return true
        } else {
            return false
        }
    }
}
