//
//  DataAccessObject.swift
//  MVP
//
//  Created by Tim Fuqua on 8/18/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - DataAccessObject
class DataAccessObject {
    // MARK: injected vars
    private(set) var repo: Repository
    
    // MARK: inits
    init(repo: Repository) {
        self.repo = repo
    }
}
