//
//  CascadingDataAccessObject.swift
//  MVP
//
//  Created by Tim Fuqua on 8/18/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - CascadingDataAccessObject
class CascadingDataAccessObject {
    // MARK: injected vars
    private(set) var cascadingRepo: CascadingRepository
    
    // MARK: inits
    init(cascadingRepo: CascadingRepository) {
        self.cascadingRepo = cascadingRepo
    }
}
