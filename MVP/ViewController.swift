//
//  ViewController.swift
//  MVP
//
//  Created by Tim Fuqua on 8/17/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: injected vars
    private var presenter: DateConverterPresenterType!
    
    // MARK: private vars
    private var keyboardOffset: CGFloat = 0
    
    // MARK: @IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iso8601Label: UILabel!
    @IBOutlet weak var epochInputTextField: UITextField!
    @IBOutlet weak var refreshButton: UIButton!
    
}

// MARK: life cycle
extension ViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        prefixPrint()

        // Normally, this would be injected into VC by Router
        presenter = DateConverterPresenterFactory.getPresenter()
        presenter.delegate = self
        
        epochInputTextField.delegate = self
        epochInputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        epochInputTextField.addToolbarToKeyboard(
            leftButton: UIBarButtonItem(title: "Dismiss", style: .plain, target: self) {
                _ = self.epochInputTextField.resignFirstResponder()
            }
            , rightButton: nil
        )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        prefixPrint()
        
        updateUI(presenter: presenter)
        presenter.inputs.didAppear()
    }
}

// MARK: @IBActions
extension ViewController {
    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        prefixPrint()
        
        presenter.inputs.refreshButtonPressed()
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        prefixPrint(message: "\(sender.text ?? "")")
        
        guard let senderText = sender.text, let epoch = Int(senderText) else { return }
        presenter.inputs.epochRawInputChanged(to: epoch)
    }
}

// MARK: initializers
extension ViewController {
    private func updateUI(presenter: DateConverterPresenterType) {
        prefixPrint()
        
        dateLabel.text = presenter.outputs.dateText
        iso8601Label.text = presenter.outputs.iso8601Text
        epochInputTextField.text = "\(presenter.outputs.epochTruncated)"
        datePicker.setDate(Date(timeIntervalSince1970: TimeInterval(presenter.outputs.epochRaw)), animated: true)
        timePicker.setDate(Date(timeIntervalSince1970: TimeInterval(presenter.outputs.epochRaw)), animated: true)
    }
}

extension ViewController: DateConverterPresenterOutputsDelegate {
    func epochDidChange(_ presenter: DateConverterPresenterType) {
        prefixPrint()
        
        DispatchQueue.main.async {
            self.updateUI(presenter: presenter)
        }
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboardOffset = (UIScreen.main.bounds.height - (textField.frame.origin.y + textField.frame.height))
        view.frame = view.frame.offsetBy(dx: 0, dy: -keyboardOffset)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        view.frame = view.frame.offsetBy(dx: 0, dy: keyboardOffset)
    }
}
