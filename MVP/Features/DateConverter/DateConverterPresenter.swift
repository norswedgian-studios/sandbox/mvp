//
//  DateConverterPresenter.swift
//  MVP
//
//  Created by Tim Fuqua on 8/18/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - DateConverterPresenterInputs
protocol DateConverterPresenterInputs {
    func didAppear()
    func refreshButtonPressed()
    func epochRawInputChanged(to epoch: Int)
}

// MARK: - DateConverterPresenterOutputs
protocol DateConverterPresenterOutputs {
    // MARK: Model
    var dateText: String { get }
    var iso8601Text: String { get }
    var epochRaw: TimeInterval { get }
    var epochTruncated: Int { get }
}

// MARK: - DateConverterPresenterOutputsDelegate
protocol DateConverterPresenterOutputsDelegate: AnyObject {
    // MARK: Model
    func epochDidChange(_ presenter: DateConverterPresenterType)
}

// MARK: - DateConverterPresenterOutputsDelegate default implementations
extension DateConverterPresenterOutputsDelegate {
    // MARK: Model
    func epochDidChange(_ presenter: DateConverterPresenterType) {}
}

// MARK: - DateConverterPresenterType
protocol DateConverterPresenterType {
    var inputs: DateConverterPresenterInputs { get }
    var outputs: DateConverterPresenterOutputs { get }
    var delegate: DateConverterPresenterOutputsDelegate? { get set }
}

// MARK: -
// MARK: -
// MARK: - DateConverterPresenter
private class DateConverterPresenter: DateConverterPresenterType, DateConverterPresenterInputs, DateConverterPresenterOutputs {
    // MARK: DateConverterPresenterType vars
    var inputs: DateConverterPresenterInputs { self }
    var outputs: DateConverterPresenterOutputs { self }
    weak var delegate: DateConverterPresenterOutputsDelegate?
    
    // MARK: DateConverterPresenterOutputs model vars
    var dateText: String { dateFromEpoch.localTimeShort }
    var iso8601Text: String { dateFromEpoch.iso8601 }
    var epochRaw: TimeInterval { dataAccessObject.model?.epoch ?? 0 }
    var epochTruncated: Int { Int(epochRaw) }
    
    // MARK: DateConverterPresenterOutputs Logic vars
    
    // MARK: injected vars
    private var dataAccessObject: DateConverterDAO
    
    // MARK: private vars
    private var dateFromEpoch: Date { Date(timeIntervalSince1970: epochRaw) }
    
    // MARK: init
    init(
        dataAccessObject: DateConverterDAO,
        delegate: DateConverterPresenterOutputsDelegate? = nil
        ) {
        self.dataAccessObject = dataAccessObject
        self.delegate = delegate
    }
}

extension DateConverterPresenter {
    private func updateEpoch(to epoch: TimeInterval) {
        prefixPrint()
        
        dataAccessObject.model?.epoch = epoch
        delegate?.epochDidChange(self)
    }
}

// MARK: - DateConverterPresenter: DateConverterPresenterInputs
extension DateConverterPresenter {
    func didAppear() {
        prefixPrint()
    }
    
    func refreshButtonPressed() {
        prefixPrint()
        
        updateEpoch(to: Date().timeIntervalSince1970)
    }
    
    func epochRawInputChanged(to epoch: Int) {
        prefixPrint()
        
        updateEpoch(to: TimeInterval(epoch))
    }
}

// MARK: - DateConverterPresenter: DateConverterPresenterOutputs
extension DateConverterPresenter {
}

// MARK: -
// MARK: -
// MARK: - DateConverter Constants and Mock
enum DateConverter {
    enum Real {
        private static let localDefaultsRepo: DictionaryRepository = {
            let model = DateConverterModel(
                epoch: 0
            )
            
            return DictionaryRepository(
                dict: [
                    DateConverterDAO.Keys.model.repoKey: model
                ]
            )
        }()
        
        static var dataAccessObject = DateConverterDAO(
            cascadingRepo: CascadingRepository(
                prioritizedRepos: [
                    UserDefaultsRepository.default,
                    localDefaultsRepo
                ]
            )
        )
    }
}

private let realPresenter: DateConverterPresenterType = DateConverterPresenter(
    dataAccessObject: DateConverter.Real.dataAccessObject
)

// MARK: -
// MARK: -
// MARK: - DateConverterPresenterFactory
enum DateConverterPresenterFactory {
    typealias PresenterFactoryType = DateConverterPresenterType
    fileprivate typealias PresenterFactoryConcreteType = DateConverterPresenter
    typealias DAOType = DateConverterDAO
    typealias OutputsDelegateFactoryType = DateConverterPresenterOutputsDelegate

    // MARK: PresenterType
    enum PresenterType {
        case real
    }
    
    static func getPresenter() -> PresenterFactoryType {
        return realPresenter
    }
    
    static func getPresenter(
        type: PresenterType
    ) -> PresenterFactoryType {
        switch type {
        case .real: return realPresenter
        }
    }
    
    static func makePresenter(
        type: PresenterType,
        delegate: OutputsDelegateFactoryType? = nil
    ) -> PresenterFactoryType {
        switch type {
        case .real:
            return PresenterFactoryConcreteType(
                dataAccessObject: DateConverter.Real.dataAccessObject,
                delegate: delegate
            )
        }
    }
    
    static func makePresenter(
        dataAccessObject: DAOType,
        delegate: OutputsDelegateFactoryType? = nil
    ) -> PresenterFactoryType {
        return PresenterFactoryConcreteType(
            dataAccessObject: dataAccessObject,
            delegate: delegate
        )
    }
}
