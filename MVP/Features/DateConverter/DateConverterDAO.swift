//
//  DateConverterDAO.swift
//  MVP
//
//  Created by Tim Fuqua on 8/18/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - DateConverterDAO
class DateConverterDAO: CascadingDataAccessObject {
    enum Keys {
        case model
        
        private var base: String { "date_converter_" }
        private var key: String {
            switch self {
            case .model: return "model"
            }
        }
        
        var repoKey: String { base + key }
    }
    
    var model: DateConverterModel? {
        get { cascadingRepo.value(forKey: Keys.model.repoKey) }
        set { cascadingRepo.update(encodableValue: newValue, forKey: Keys.model.repoKey) }
    }
}
