//
//  DateConverterModel.swift
//  MVP
//
//  Created by Tim Fuqua on 8/18/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

struct DateConverterModel: Codable, Equatable {
    var epoch: TimeInterval?
}
